fun main(args: Array<String>) {
    val kaosKaki = PasanganKaosKaki()
    kaosKaki.first()
    kaosKaki.second()
}

class PasanganKaosKaki() {
    fun first() {
        val arr = arrayOf(10, 20, 20, 10, 10, 30, 50, 10, 20)
//        val arr = arrayOf(1, 2, 1, 2, 1, 3, 2)
        val valueArr = arr.distinct()
        val map = hashMapOf<Int, Int>()
        var totalPair = 0
        valueArr.forEach { target ->
            map[target] = arr.count { target == it }
        }
        map.forEach { (index, value) ->
            var i = value - 2
            var pair = 0
            while (i > -1) {
                pair++
                i -= 2
            }
            totalPair += pair
            println("Kaos Kaki $index memiliki $pair pasangan dan tersisa ${value % 2}")
        }
        println("Total pasangan adalah $totalPair")

    }

    fun second() {
        val arr = arrayOf(1, 2, 3, 4)
//        val arr = arrayOf(1, 12, 5, 111, 200, 1000, 10)
        arr.sort()
        val max = 7
//        val max = 50
        val map = HashMap<Int, ArrayList<Int>>()
        val resultArr = arrayListOf<Int>()
        var solved = false
        var index = arr.size - 1
        while (!solved) {
            var current = 0
            var total = 0
            val jumlah = arrayListOf<Int>()
            for (i in index downTo 0) {
                current += arr[i]
                if (current >= max) {
                    break
                } else {
                    jumlah.add(arr[i])
                    total++
                }
            }
            map[total] = jumlah
            resultArr.add(total)
            index--
            solved = index <= 0
        }
        println("cara paling optimal adalah ${resultArr.max()} dengan total ${map[resultArr.max()].toString()}")
    }
}